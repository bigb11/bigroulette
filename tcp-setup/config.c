#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include "config.h"


#define PORT 8080
#define IP "127.0.0.1"


//* Connection Setup
//* - Building struct sockadd_in parameters based on "type" variable:
//      if "type" is true => config of server sockaddr_in 
//      else => config of client sockaddr_in 
//* - Returns:
//      socketfd: id of socked created
int connectionConfiguration(struct sockaddr_in *addr, bool type){
    
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    assert(sockfd != -1);

    bzero(addr, sizeof((*addr)));

    // assign IP, PORT
    (*addr).sin_family = AF_INET;
    (*addr).sin_port = htons(PORT);

    if(type) // true => server address
        (*addr).sin_addr.s_addr = htonl(INADDR_ANY);
    else    // false => client address
        (*addr).sin_addr.s_addr = inet_addr(IP);

    return sockfd;
}