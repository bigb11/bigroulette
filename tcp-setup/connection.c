#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <time.h>
#include "connection.h"

#define SA struct sockaddr


// startConnection: setup bind(), listen and servAddr
void startConnection(int sd,  struct sockaddr_in servAddr){
    // Binding newly created socket to given IP and verification
    if ((bind(sd, (SA*)&servAddr, sizeof(servAddr))) != 0) {
        printf("\nsocket bind failed...\n");
        exit(0);
    }
    else
        printf("\nSocket successfully binded..\n");
  
    // Now server is ready to listen and verification
    if ((listen(sd, 5)) != 0) {
        printf("\nListen failed...\n");
        exit(0);
    }
    else
        printf("\nServer listening..\n");

}


// acceptConnection: allows client to connect and return the socket client
int acceptConnection(int sd, struct sockaddr_in clientAddr){
    
    socklen_t len = sizeof(clientAddr);

    int connfd = accept(sd, (SA*)&clientAddr, &len);
    assert(connfd >= 0);
    printf("server acccept the client...\n");

    return connfd;
}
