#ifndef connection_h
#define connection_h


void startConnection(int sd,  struct sockaddr_in servAddr);

int acceptConnection(int sd, struct sockaddr_in clientAddr);


#endif