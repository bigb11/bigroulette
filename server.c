#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <time.h>
#include "./tcp-setup/connection.c"
#include "./tcp-setup/config.c"




#define MAX 80
#define PORT 8080
#define NMAX_PLAYER 126
#define SA struct sockaddr



int main(int argc, char const *argv[])
{
    int sd, connfd;
    struct sockaddr_in servAddr, clientAddr;

    FILE *fp;

    fp = fopen("./database/login.txt","w");
    fclose(fp);

    sd = connectionConfiguration(&servAddr,true);
    startConnection(sd, servAddr);

    while(true){
        //Viene creato un thread ogni volta accettata la connessione
        int clientfd=acceptConnection(sd, clientAddr);
        
    }

    //Chiusura del socket
    close(sd);

    return 0;
}
